const nodemailer = require("nodemailer")
const ejs = require("ejs")
let path = require('path');
require('dotenv').config()

let template_path=path.join(__dirname, 'templates')

async function sendMail(dest, content, mail, test=false){
    let email, password,host
    let file_path=path.join(template_path, `${mail.type}.ejs`)
    if (test){
        let testAccount = await nodemailer.createTestAccount();
        host="smtp.ethereal.email"
        email=testAccount.user
        password=testAccount.pass
    }else{
        host='smtp.gmail.com'
        email=process.env.SMTP_USER
        password=process.env.MAIL_PASSWORD
    }

    const transporter = nodemailer.createTransport({
        host: host,
        port: 587,
        auth: {
            type: "login", // default
            user: email,
            pass: password
        }
    })

    ejs.renderFile(file_path, mail.data, (error, output) => {
        if (error){throw error}
        transporter.sendMail({
            from: `Comp Júnior <${email}>`, // sender address
            to: dest.email, // list of receivers
            subject: content.subject, // Subject line
            text: content.text, // plain text body
            html: output, // html body
        }).then((info)=>{
            console.log("Message sent: %s", info.messageId)
        }).catch((err)=>{console.error})
     })

    

}

module.exports=sendMail

