const aws = require('aws-sdk')
const multer = require('multer')
const multerS3 = require('multer-s3')
const path=require('path')
require('dotenv').config()


aws.config.update({
    secretAccessKey: process.env.AWS_SECRET_KEY,
    accessKeyId: process.env.AWS_ACCESS_KEY,
    region: process.env.AWS_REGION_NAME
});

const s3 = new aws.S3();

const upload = multer({
    storage: multerS3({
        s3: s3,
        bucket: process.env.AWS_BUCKET_NAME,
        key: function (req, file, cb) {
            cb(null, `${req.body.username}${path.extname(file.originalname)}`)
        }
    })
});

module.exports=upload