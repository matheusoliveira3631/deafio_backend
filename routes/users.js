const express = require('express')
const upload=require('..//utils/upload')
const Controller = require('../controllers/userController')
const Middleware = require('../middleware/userMiddleware')
const userController = new Controller
const userMiddleware = new Middleware

const router = express.Router()

router.get('/', userController.serialize)

router.post('/', upload.single('file'),userMiddleware.bodyValidation,userController.createUser)

router.get('/register/:username', userController.confirmUserAccount)

router.get('/reminder', userController.passwordReminder)

router.get('/find', userController.getUserBy)

router.put('/:username', userMiddleware.username,userController.editUser)

router.delete('/:username', userMiddleware.username,userController.deleteUser)

module.exports = router