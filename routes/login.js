const express = require('express')
const Controller = require('../controllers/userController')
const Middleware = require('../middleware/userMiddleware')
const userController = new Controller
const userMiddleware = new Middleware

const router = express.Router()

router.post('/', userMiddleware.login, (req, res)=>{
    res.status(201).json({message:'ok'})
})

router.post('/admin', userMiddleware.login, userMiddleware.isAdmin, (req, res)=>{
    res.status(201).json({message:'ok'})
})


module.exports = router