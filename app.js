let express = require('express')
let path = require('path')
let cookieParser = require('cookie-parser')
let logger = require('morgan')
const cors = require('cors')
const swaggerJSDoc = require('swagger-jsdoc');  
const swaggerUI = require('swagger-ui-express');  

//routes
let userRouter=require('./routes/users')
let loginRouter = require('./routes/login')
//Swagger Configuration  
const swaggerOptions = {
    definition: {
      openapi: "3.0.0",
      info: {
        title: "API de registro de usuário",
        version: "0.1.0",
        description:
          "API CRUD simples para registrar e atenticar usuários como parte do desafio de capacitação backend da CompJúnior",
        license: {
          name: "MIT",
          url: "https://spdx.org/licenses/MIT.html",
        },
        contact: {
          name: "Matheus Henrique",
          email: "matheus.oliveira@compjunior.com.br",
        },
      },
    },
    apis: ["./swagger.js"],
  }; 

const swaggerDocs = swaggerJSDoc(swaggerOptions)
const app = express()

app.use(logger('dev'))
app.use(express.json())
app.use(express.urlencoded({ extended: true }))
app.use(cookieParser())
app.use(cors())

app.use('/users', userRouter)
app.use('/login', loginRouter)
app.use('/docs',swaggerUI.serve,swaggerUI.setup(swaggerDocs))

module.exports = app;
