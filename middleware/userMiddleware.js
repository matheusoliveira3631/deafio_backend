const {getUserBy, Serialize} = require('../database/methods/findUser')
const {hashPassword, checkPassword} = require('../utils/hash')

module.exports = class userMiddleware {
    //checa se todos os atributos necessários para a criação do usuários estão no body
    async bodyValidation(req, res, next){
        const body = req.body
        //remove later
        console.log(req.body)
        const emptyFields = []
        body.username ? {} : emptyFields.push('Username')
        body.email ? {} : emptyFields.push('Email')
        body.password ? {} : emptyFields.push('Password')
        body.admin ? {} : emptyFields.push('Admin')
        if(emptyFields.length){
            res.status(500).json({message:`Required fields: ${emptyFields}`})
        }else{
            hashPassword(body.password, function(pwd){
                body.password=pwd
                next()
            })
            
    }}

    //checa se o usuário a ser editado existe antes de prossegui para o método de edição
    async username(req, res, next){
        let username = null
        req.params.username ?  username = req.params.username : username = req.body['username']
        const result = await getUserBy('username', username)
        result ? next() : res.status(500).json({message:`Não há usuário com o username de ${username}`})
    }

    async login(req, res, next){
        let param, value
        console.log(req.body)
        if(req.body['username']){
            param='username'
            value=req.body['username']
        }else{
            param='email',
            value=req.body['email']
        }
        const plainTextPassword = req.body['password'].toString()
        getUserBy(param, value).then((user)=>{
            const hashedPassword = user.password.toString()
            checkPassword(plainTextPassword, hashedPassword, (bool)=>{
                bool ? next() : res.status(401).json({message:'Senha incorreta'})
            })
        }).catch(err=>{
            res.status(401).json({message:'No user found'})
        })
    }

    async isAdmin(req, res, next){
        const user=await getUserBy('username', req.body.username)
        user.admin ? next() : res.status(403).json({message:"Usuário não tem privilégos de administrador"})
    }
}