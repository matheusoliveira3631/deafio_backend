## Desafio de capacitação backend CompJúnior
<!-- blank -->
Projeto feito como parte da capacitação backend do período de trainee da CompJúnior
<!-- blank -->
----
<!-- blank -->
## Instalação

### MongoDB
Para uso local é **necessário** ter uma instalação funcional do MongoDB
#### Windows
[MongoDB community][https://www.mongodb.com/try/download/community]
#### Linux
`sudo apt install mongodb`
<!-- blank -->
----
<!-- blank -->
## Dependências
Para instalar as dependências Node.js use `npm install` na pasta raíz do projeto, a mesma que contém o arquivo ***package.json***
<!-- blank -->
----
<!-- blank -->
## Vaiáveis de desenvolvimento
SMTP_USER=email com SMTP ativado para envio dos emails automáticos <br>
MAIL_PASSWORD=senha do email <br>
AWS_ACCESS_KEY=chave de acesso aws <br>
AWS_SECRET_KEY=chave secreta aws <br>
AWS_BUCKET_NAME=nome de um bucket s3 válido para depósito de imagens <br>
AWS_REGION_NAME=região do servidor/bucket, exemplo:us-east-1

