/**
 * @swagger
 *  components:
 *      schemas:
 *          User:
 *              type: object
 *              required:
 *                  - username
 *                  - email
 *                  - password
 *                  - admin
 *              properties:
 *                  username:
 *                      type: integer
 *                      description: Nome de usuário
 *                  email:
 *                      type: string
 *                      description: Email do usuário
 *                  password:
 *                      type: string
 *                      description: senha do usuário (plaintext, será transformada em hash)
 *                  admin:
 *                      type: boolean
 *                      description: Define se o usário possui privilégios de administrador
 *                  accountConfirmed:
 *                      type: boolean
 *                      description: Define se a conta do usuário foi cofirmada via email de confirmação
 *              example:
 *                  username: José1234
 *                  email: jose@email.com
 *                  password: josemaria123
 *                  admin: false
 *                  accountConfirmed: false
 *       
 */
/**
 * @swagger
 *  tags:
 *      
 *      - name: Users
 *        description: Rotas que envolvem dados/manipulação de usuários
 * 
 *      - name: Login
 *        description: Rotas que envolvem autenticação
 * 
 */
/**
 * @swagger
 * paths:
 *      /users/:
 *          get:
 *              summary: Retorna todos os usuários cadastrados
 *              tags: [Users]
 *              responses:
 *                  "200":
 *                      description: Lista de usuários cadastrados
 *                      content:
 *                          application/json:
 *                              schema:
 *                                  $ref: '#/components/schemas/User'
 *          post:
 *              summary: Cria um novo usuário, imagem pode ser enviada utilizando form-data, o campo do arquivo deve obrigatoriamente ter o nome "file"
 *              tags: [Users]
 *              requestBody:
 *                  required: true
 *                  content:
 *                      application/json:
 *                          schema:
 *                              $ref: "#/components/schemas/User"
 *              responses:
 *                  "200":
 *                      description: Confirmação de sucesso
 *                      content: 
 *                          application/json:
 *                              schema:
 *                                  $ref: "#/components/schemas/User"
 * 
 *      /users/{username}:
 *          put:
 *              summary: Atualiza atributos de um usuário de acordo com o nome de usuário
 *              tags: [Users]
 *              requestBody:
 *                  required: true
 *                  content:
 *                      application/json:
 *                          schema:
 *                              type: object
 *                              properties:
 *                                  param:
 *                                      type: string
 *                                      description: Parâmetro a ser modificado
 *                                  old:
 *                                      type: string
 *                                      description: Valor atual
 *                                  new:
 *                                      type: string
 *                                      description: Novo valor
 *              responses:
 *                  "200":
 *                      description: Confirmação de sucesso
 *                      content: 
 *                          text/plain:
 *                              schema:
 *                                  type: string
 *                                  example: "ok"
 *                                      
 *          delete:
 *              summary: Deleta um usuário com base no username 
 *              tags: [Users]
 *              responses:
 *                  "200":
 *                      description: Confirmação de sucesso
 *                      content: 
 *                          text/plain:
 *                              schema:
 *                                  type: string
 *                                  example: "ok"
 *       
 *      /users/find/:
 *          get:
 *              summary: Procura um usuário com base nos parâmetros fornecidos
 *              tags: [Users]
 *              requestBody:
 *                  required: true
 *                  content:
 *                      application/json:
 *                          schema:
 *                              type: object
 *                              properties:
 *                                  param:
 *                                      type: string
 *                                      example: "email"
 *                                  value:
 *                                      type: string
 *                                      example: "user@mail.com"
 *              responses:
 *                  "200":
 *                      description: Usuário encontrado
 *                      content: 
 *                          application/json:
 *                              schema:
 *                                  $ref: "#/components/schemas/User"
 * 
 *            
 *      /users/register/{username}:
 *          get:
 *              summary: Confirma a conta do usuário mudando o campo accountConfirmed para true
 *              tags: [Users]
 *              responses:
 *                  "200":
 *                      description: Confirmação de sucesso
 *                      content: 
 *                          text/plain:
 *                              schema:
 *                                  type: string
 *                                  example: "ok"
 * 
 *      /users/reminder/{username}:
 *          get:
 *              summary: Envia um email de redefinição de senha para o email cadastrado do usuário
 *              tags: [Users]
 *              responses:
 *                  "200":
 *                      description: Confirmação de sucesso
 *                      content: 
 *                          text/plain:
 *                              schema:
 *                                  type: string
 *                                  example: "ok"
 * 
 *      /login/:
 *          post:
 *              summary: Autentica usuários
 *              tags: [Login]
 *              requestBody:
 *                  required: true
 *                  content:
 *                      application/json:
 *                          schema:
 *                              type: object
 *                              properties:
 *                                  username:
 *                                      type: string
 *                                      description: Nome de usuário
 *                                  password:
 *                                      type: string
 *                                      description: Senha
 * 
 *              responses:
 *                  "200":
 *                      description: Confirmação de sucesso
 *                      content: 
 *                          text/plain:
 *                              schema:
 *                                  type: string
 *                                  example: "ok"
 *                  "401":
 *                      description: Senha incorreta
 *                      content:
 *                          application/json:
 *                              schema:
 *                                  type: object
 *                                  properties:
 *                                      message:
 *                                          type: string
 *                                          description: Mensagem de erro
 *                                          example: "Senha incorreta"
 * 
 * 
 * 
 *      /login/admin/:
 *          post:
 *              summary: Autentica o usuário e verifica se possui privilégios de admin
 *              tags: [Login]
 *              requestBody:
 *                  required: true
 *                  content:
 *                      application/json:
 *                          schema:
 *                              type: object
 *                              properties:
 *                                  username:
 *                                      type: string
 *                                      description: Nome de usuário
 *                                  password:
 *                                      type: string
 *                                      description: Senha
 * 
 *              responses:
 *                  "200":
 *                      description: Confirmação de sucesso
 *                      content: 
 *                          text/plain:
 *                              schema:
 *                                  type: string
 *                                  example: "ok"
 *                  "401":
 *                      description: Senha incorreta
 *                      content:
 *                          application/json:
 *                              schema:
 *                                  type: object
 *                                  properties:
 *                                      message:
 *                                          type: string
 *                                          description: Mensagem de erro
 *                                          example: "Senha incorreta"
 *                  "403":
 *                      description: Usuário não possui privilégios de administrador
 *                      content:
 *                          aplication/json:
 *                              schema:
 *                                  type: object
 *                                  properties:
 *                                      message:
 *                                          type: string
 *                                          description: Mensagem de erro
 *                                          example: "Usuário não possui privilégios de admin"                
 */