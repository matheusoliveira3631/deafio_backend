const {deleteUser, editUser, registerUser} = require('../database/methods/editUser')
const {getUserBy, Serialize} = require('../database/methods/findUser')
const sendMail = require('../utils/mail/mail')
const user=require('../database/schema/user')

module.exports = class employeeController {
    async serialize(req, res){
        try{
            Serialize().then((userList)=>{
                res.status(200).json(userList)
            })
        } catch(err){
            res.status(500).json({message:err})
        }
    }


    async getUserBy(req, res){
        const {param, value} = req.body
        const doc = await user.findOne({param:value})
        if(doc){
            res.status(200).json(doc)
        }else{
            res.status(409).json({message:'Not found'})
        }
    }

    async createUser(req, res){
        try{
            const {username, password, email, admin} = req.body
            if(await user.findOne({username:username}) || await user.findOne({email:email})){
                res.status(409).json({message:'User already exists'})
            }else{
            user.create({
                username,
                password,
                email,
                admin
            }, (err, doc)=>{
                if(err){res.status(500).json({message:err})}
                //envia um email de confirmação para o usuário
                sendMail(
                    {name:req.body.username, email:req.body.email}, 
                    {subject:'Registro', text:"Confirme sua conta"}, 
                    {type:'register', data:{name:req.body.username}}
                )
                res.status(201).json(doc)
            }) 
        }
        } catch(err){
            console.log(err)
            res.status(500).json({message:err})
        }
    }

    async editUser(req, res){
        try{
            await editUser(req.body.param, {'old':req.body.old, 'new':req.body.new})
            res.status(200).json({message:'ok'})
        }catch(err){
            res.status(500).json({message:err})
        }
    }

    async deleteUser(req, res){
        try{
            await deleteUser(req.params.username)
            res.status(200).json({message:'ok'})
        }catch(err){
            res.status(500).json({message:err})
        }
    }

    async confirmUserAccount(req, res){
        try{
            await registerUser(req.params.username)
            res.status(201).json({message:'ok'})
        }catch(err){
            res.status(500).json({message:err})
        }
    }

    async passwordReminder(req, res){
        try{
            const email=req.query.email
            const doc = await user.findOne({email:email})
            if(doc){
                sendMail(
                    {name:req.params.username, email:doc.email}, 
                    {subject:'Senha', text:"Recuperação de senha"}, 
                    {type:'password', data:{name:req.params.username}}
                )
                res.status(201).json({message:'ok'})
            }else{
                res.status(404).json({message:"User doesn't exists"})
            }   
        }catch(err){
           res.status(500).json({message:err})
        }
    }
}