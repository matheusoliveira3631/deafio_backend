const mongoose=require('mongoose')
const schema=mongoose.Schema

const UserSchema=new schema({
    username:{
        type:String,
        required:true,
    },
    email:{
        type:String,
        required:true
    },
    password:{
        type:String,
        required:true
    },
    admin:{
        type:Boolean,
        required:true
    },
    accountConfirmed:{
        type:Boolean,
        required:false,
        default:false
    }
})

const user= mongoose.model('user', UserSchema)

module.exports = user