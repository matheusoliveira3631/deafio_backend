const user = require('../schema/user')

//retorna todos os usuários do banco
const Serialize = async function(){
    const response=[]
    for await (const doc of user.find({})){
        response.push(doc)
    }
    return response
}

const getUserBy= async (param, value)=>{
    const doc = await user.findOne({param:value})
    if(doc){
        return doc
    }else{
        return false
    }
}

module.exports = {getUserBy, Serialize}