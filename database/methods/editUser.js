const user = require('../schema/user')

//deleta usuário baseado em paraâmetros de query
const deleteUser = async function(username){
    try{
        user.deleteOne({'username':username}, (err)=>{
            if (err){
                throw err
            }else{
                return 'ok'
            }
        })
    }catch(err){
        return err
    }
}

//edita usuário
//exemplo de chamada: updateUser(username, {old:'Marcos123', 'new':'MarcosNovo'})
const editUser = async function(param, values){
    const query={}
    const update={}
    query[param]=values['old']
    update[param]=values['new']
    try{
    user.findOneAndUpdate(query, update, (err)=>{
        if (err){
            console.log(err)
        }else{
            return 'ok'
        }
    })
    }catch(err){
        return err
    }
}

const registerUser = async function(username){
    user.findOneAndUpdate({username:username}, {accountConfirmed:true}, (err)=>{
        if (err){
            console.log(err)
        }else{
            return 'ok'
        }
    })
}


module.exports = {deleteUser, editUser, registerUser}